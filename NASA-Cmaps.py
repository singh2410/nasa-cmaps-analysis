#!/usr/bin/env python
# coding: utf-8

# # NASA-Cmaps:EDA
# #By- Aarush Kumar
# #Dated: August 17,2021

# In[1]:


from IPython.display import Image
Image(url='https://www.thenewsminute.com/sites/default/files/styles/news_detail/public/Earth_at_night_NASA.jpg?itok=PGHkA3uM')


# In[2]:


import numpy as np 
import pandas as pd


# In[3]:


data = pd.read_csv("/home/aarush100616/Downloads/Projects/Nasa Cmaps/CMaps/train_FD001.txt", sep=" ", header=None)


# In[4]:


data


# In[5]:


COLS = ["ID", "Cycle", "OpSet1", "OpSet2", "OpSet3", "SensorMeasure1", "SensorMeasure2", "SensorMeasure3", "SensorMeasure4",
                "SensorMeasure5", "SensorMeasure6", "SensorMeasure7", "SensorMeasure8", "SensorMeasure9", "SensorMeasure10", "SensorMeasure11",
                "SensorMeasure12", "SensorMeasure13", "SensorMeasure14", "SensorMeasure15", "SensorMeasure16",
                "SensorMeasure17", "SensorMeasure18", "SensorMeasure19", "SensorMeasure20", "SensorMeasure21"]


# In[6]:


data = data[[f for f in range(0, 26)]]
data.columns = ["ID", "Cycle", "OpSet1", "OpSet2", "OpSet3", "SensorMeasure1", "SensorMeasure2", "SensorMeasure3", "SensorMeasure4",
                "SensorMeasure5", "SensorMeasure6", "SensorMeasure7", "SensorMeasure8", "SensorMeasure9", "SensorMeasure10", "SensorMeasure11",
                "SensorMeasure12", "SensorMeasure13", "SensorMeasure14", "SensorMeasure15", "SensorMeasure16",
                "SensorMeasure17", "SensorMeasure18", "SensorMeasure19", "SensorMeasure20", "SensorMeasure21"]


# In[7]:


data


# In[8]:


data[data["ID"]==1]


# In[9]:


max_cycles_df = data.groupby(["ID"], sort=False)["Cycle"].max().reset_index().rename(columns={"Cycle" : "MaxCycleID"})
max_cycles_df.head()


# In[10]:


# Remaining Useful Life (RUL) 
FD001_df = pd.merge(data, max_cycles_df, how="inner", on="ID")
FD001_df["RUL"] = FD001_df["MaxCycleID"] - FD001_df["Cycle"]
FD001_df


# In[11]:


FD001_df.to_csv("FD001.csv", index=None)


# In[12]:


import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler


# In[13]:


df_mini = FD001_df.copy().drop(columns=["ID", "Cycle", "OpSet1", "OpSet2", "OpSet3", "MaxCycleID", "RUL"], axis=1)
scaler = MinMaxScaler(feature_range=(0, 1))
print(df_mini)
scaled_df = pd.DataFrame(scaler.fit_transform(df_mini.values))
scaled_df = FD001_df[["ID", "Cycle", "RUL"]].join(scaled_df)
scaled_df.columns = ["ID", "Cycle","RUL", "SensorMeasure1", "SensorMeasure2", "SensorMeasure3", "SensorMeasure4",
                "SensorMeasure5", "SensorMeasure6", "SensorMeasure7", "SensorMeasure8", "SensorMeasure9", "SensorMeasure10", "SensorMeasure11",
                "SensorMeasure12", "SensorMeasure13", "SensorMeasure14", "SensorMeasure15", "SensorMeasure16",
                "SensorMeasure17", "SensorMeasure18", "SensorMeasure19", "SensorMeasure20", "SensorMeasure21"]


# In[14]:


df_mini.values


# In[15]:


scaled_df


# In[16]:


print("SCALED")
plt.figure()
scaled_df.plot.scatter(x="Cycle", y=["SensorMeasure4"])
plt.show()

print("ORIGINAL")
plt.figure()
data.plot.scatter(x="Cycle", y=["SensorMeasure4"])
plt.show()


# In[17]:


plt.style.use("seaborn")
sensor_count = 21
engine_id = 2
for i in range(1, sensor_count+1):
    y_value = "SensorMeasure" + str(i)
    scaled_df[scaled_df["ID"]==engine_id].plot(x="Cycle", y=y_value)


# In[18]:


scaled_clean_df = scaled_df.drop(columns=["SensorMeasure1", "SensorMeasure5", "SensorMeasure6", "SensorMeasure10",
                                         "SensorMeasure16", "SensorMeasure18", "SensorMeasure19"])
scaled_clean_df


# In[19]:


cycle_counts = scaled_clean_df.groupby("ID")["Cycle"].max().reset_index()


# In[20]:


cycle_counts.plot(x="ID", y=["Cycle"], kind="hist")
cycle_counts.plot.scatter(x="Cycle", y=["ID"])


# In[21]:


standart_deviations = scaled_clean_df.groupby("ID").std().reset_index().drop(columns=["Cycle"])
standart_deviations


# In[22]:


engine_id = 1
standart_deviations[standart_deviations["ID"]==engine_id].plot(y=["SensorMeasure2","SensorMeasure3","SensorMeasure4","SensorMeasure7",
                                                                 "SensorMeasure8","SensorMeasure9","SensorMeasure11","SensorMeasure12",
                                                                 "SensorMeasure13","SensorMeasure14","SensorMeasure15","SensorMeasure17",
                                                                 "SensorMeasure20","SensorMeasure21"], kind="bar", figsize=(10, 5))


# In[23]:


sensors = ["SensorMeasure2","SensorMeasure3","SensorMeasure4","SensorMeasure7",
           "SensorMeasure8","SensorMeasure9","SensorMeasure11","SensorMeasure12",
           "SensorMeasure13","SensorMeasure14","SensorMeasure15","SensorMeasure17",
           "SensorMeasure20","SensorMeasure21"]

engine_to_check = 1

scaled_clean_df[scaled_clean_df["ID"]==engine_to_check].plot(x="Cycle", y=sensors, figsize=(20, 10))

rul_fixed_df = scaled_clean_df.copy()
rul_fixed_df.loc[rul_fixed_df["RUL"] < 20, "RUL"] = 0
rul_fixed_df.loc[rul_fixed_df["RUL"] >= 20, "RUL"] = 1
rul_fixed_df
col = rul_fixed_df[rul_fixed_df["ID"]==engine_to_check].RUL.map({1:'b', 0:'r'})
rul_fixed_df[rul_fixed_df["ID"]==engine_to_check].plot.scatter(x=sensors[5], y=[sensors[9]], c=col)
rul_fixed_df[rul_fixed_df["ID"]==engine_to_check].plot.scatter(x=sensors[2], y=[sensors[6]], c=col)
rul_fixed_df[rul_fixed_df["ID"]==engine_to_check].plot.scatter(x=sensors[12], y=[sensors[13]], c=col)


# In[24]:


import seaborn


# In[25]:


correlation_df = scaled_clean_df.drop(columns=["ID", "Cycle", "RUL"])
corr_default = correlation_df.corr()
corr_default


# In[26]:


plt.figure(figsize=(12, 10))
seaborn.heatmap(corr_default, annot=True, cmap="rocket")


# In[27]:


correlation_df = scaled_df.drop(columns=["ID", "Cycle", "RUL"])
corr_default = correlation_df.corr()
corr_default


# In[28]:


plt.figure(figsize=(12, 10))
seaborn.heatmap(corr_default, annot=True, cmap="rocket")


# In[29]:


S1, S2 = "SensorMeasure11", "SensorMeasure12"
fig, axes = plt.subplots(2, 5, figsize=(15, 10))
fig.tight_layout()

for i in range(2):
    for j in range(5):
        df_used = scaled_clean_df[scaled_clean_df["ID"]==(5*i+j+1)]
        axes[i][j].plot(df_used["Cycle"], df_used[S1])
        axes[i][j].plot(df_used["Cycle"], df_used[S2])
        axes[i][j].title.set_text("Engine ID: " + str(5*i+j+1))

plt.show()


# In[30]:



fig, axes = plt.subplots(1, 2, figsize=(25, 10))
axes[0].title.set_text("Tüm veri kümesi")
seaborn.heatmap(scaled_clean_df.copy().drop(columns=["ID", "Cycle", "RUL"]), ax=axes[0])

engine_id = 1
axes[1].title.set_text("Motor ID: " + str(engine_id))
seaborn.heatmap(scaled_clean_df[scaled_clean_df["ID"]==engine_id].copy().drop(columns=["ID", "Cycle", "RUL"]), ax=axes[1])


# In[31]:


fig, axes = plt.subplots(1, 2, figsize=(30, 10))
axes[0].title.set_text("Çıkarılan özelliklerle tüm veri kümesi")
seaborn.heatmap(scaled_df.copy().drop(columns=["ID", "Cycle", "RUL"]), ax=axes[0])

engine_id = 1
axes[1].title.set_text("Çıkarılan özelliklerle Motor ID: " + str(engine_id))
seaborn.heatmap(scaled_df[scaled_df["ID"]==engine_id].copy().drop(columns=["ID", "Cycle", "RUL"]), ax=axes[1])


# In[32]:


class_df = scaled_clean_df.copy()
class_df['HS'] = [0 for x in range(len(class_df['RUL']))]
class_df.loc[class_df["RUL"] <= 10, "HS"] = 'Not Okay'
class_df.loc[class_df['RUL'] >= 120, 'HS'] = 'Okay'
class_df.loc[(class_df['RUL'] < 120) & (class_df['RUL'] > 10), 'HS'] = 'Degradation'
class_df


# In[33]:


HS_numeric = class_df.HS.map({'Okay':2, 'Not Okay':0, 'Degradation':1})
col = class_df.HS.map({'Okay':'b', 'Not Okay':'r', 'Degradation':'y'})
class_df['HS Numeric'] = HS_numeric
plt.figure()
class_df.plot.scatter(x="Cycle", y=["HS Numeric"], color=col)


# In[34]:


fig, axes = plt.subplots(4, 4, figsize=(15, 15))
engine = 1
sensor = "SensorMeasure9"
for i in range(4):
    for j in range(4):
        col = class_df[class_df['ID']==engine].HS.map({'Okay':'b', 'Not Okay':'r', 'Degradation':'y'})
        plt.figure()
        axes[i][j].scatter(class_df[class_df["ID"]==engine]["Cycle"], class_df[class_df["ID"]==engine][sensor], color=col)
        axes[i][j].title.set_text("Engine: " + str(engine))
        engine += 1


# In[35]:


class_df.info


# In[36]:


class_df['HS Numeric'].value_counts()


# In[37]:


X = class_df.drop([ 'ID','Cycle','RUL','HS','HS Numeric'], axis=1)
y = class_df['HS Numeric']


# In[38]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 42)


# In[39]:


X_train.shape, X_test.shape


# In[40]:


X_train.dtypes


# In[41]:


X_train.head()


# In[42]:


get_ipython().system('pip install category-encoders')


# In[43]:


import category_encoders as ce


# In[44]:


from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import DecisionTreeRegressor


# In[45]:


clf_gini = DecisionTreeClassifier(criterion='gini', max_depth=10, random_state=0)
# fit the model
clf_gini.fit(X_train, y_train)


# In[46]:


y_pred_gini = clf_gini.predict(X_test)


# In[47]:


from sklearn.metrics import accuracy_score
print('Model accuracy score with criterion gini index: {0:0.4f}'. format(accuracy_score(y_test, y_pred_gini)))


# In[48]:


accuracy_score(y_test, y_pred_gini)


# In[49]:


y_pred_train_gini = clf_gini.predict(X_train)
y_pred_train_gini


# In[50]:


print('Training-set accuracy score: {0:0.4f}'. format(accuracy_score(y_train, y_pred_train_gini)))


# In[51]:


print('Training set score: {:.4f}'.format(clf_gini.score(X_train, y_train)))
print('Test set score: {:.4f}'.format(clf_gini.score(X_test, y_test)))


# In[52]:


from sklearn import tree


# In[53]:


clf_en = DecisionTreeClassifier(criterion='entropy', max_depth=10, random_state=0)
clf_en.fit(X_train, y_train)


# In[54]:


y_pred_en = clf_en.predict(X_test)


# In[55]:


from sklearn.metrics import accuracy_score
print('Model accuracy score with criterion entropy: {0:0.4f}'. format(accuracy_score(y_test, y_pred_en)))


# In[56]:


y_pred_train_en = clf_en.predict(X_train)
y_pred_train_en


# In[57]:


print('Training-set accuracy score: {0:0.4f}'. format(accuracy_score(y_train, y_pred_train_en)))


# In[58]:


print('Training set score: {:.4f}'.format(clf_en.score(X_train, y_train)))
print('Test set score: {:.4f}'.format(clf_en.score(X_test, y_test)))


# In[59]:


plt.figure(figsize=(12,8))
from sklearn import tree
tree.plot_tree(clf_en.fit(X_train, y_train))


# In[60]:


from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.model_selection import GridSearchCV

# Choose the type of classifier. 
clf = RandomForestClassifier(random_state=42)

# Choose some parameter combinations to try
parameters = {'n_estimators': [10,20,50], 
              'max_features': ['log2', 'sqrt','auto'], 
              'criterion': ['entropy', 'gini'],
              'max_depth': [4, 5, 6, 10], 
              
             }

# Type of scoring used to compare parameter combinations
acc_scorer = make_scorer(accuracy_score)

grid_obj = GridSearchCV(estimator= clf, param_grid=parameters, cv= 5)
grid_obj.fit(X_train, y_train)
# Run the grid search
#grid_obj = GridSearchCV(clf, parameters, scoring=acc_scorer)
#grid_obj = grid_obj.fit(X_train, y_train)

# Set the clf to the best combination of parameters
clf = grid_obj.best_estimator_

# Fit the best algorithm to the data. 
clf.fit(X_train, y_train)


# In[61]:


predictions = clf.predict(X_test)
print(accuracy_score(y_test, predictions))


# In[62]:


grid_obj.best_params_


# In[63]:


rfc1=RandomForestClassifier(random_state=42, max_features='log2', n_estimators= 20, max_depth=8, criterion='gini')


# In[64]:


rfc1.fit(X_train, y_train)


# In[65]:


pred=rfc1.predict(X_train)
print("Accuracy for Random Forest on data: ",accuracy_score(y_train,pred))

